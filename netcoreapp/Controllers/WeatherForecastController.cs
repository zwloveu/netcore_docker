﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace netcoreapp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ConnectionMultiplexer _redis;
        private readonly IDatabase _database;
        private readonly IConfiguration _configuration;

        public WeatherForecastController(ILoggerFactory loggerFactory, ConnectionMultiplexer redis, IConfiguration configuration)
        {
            _logger = loggerFactory.CreateLogger<WeatherForecastController>();
            _redis = redis;
            _database = redis.GetDatabase();
            _configuration = configuration;
        }


        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            var rng = new Random();
            var range = Enumerable.Range(1, 5);
            var redisKey = range.Sum().ToString();
            var data = await _database.StringGetAsync(redisKey);

            if (data.IsNullOrEmpty)
            {
                var redisData = range.Select(index => new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(index),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                });

                var created = await _database.StringSetAsync(redisKey, JsonConvert.SerializeObject(redisData));

                if (!created)
                {
                    _logger.LogInformation("Save Failed");
                }
            }

            data = await _database.StringGetAsync(redisKey);
            return JsonConvert.DeserializeObject<IEnumerable<WeatherForecast>>(data)!;
        }
    }
}

# Create Dockerfile
* this file named "Dockerfile", under your startup project.
* make sure it is copied to the program output directory
```
touch Dockerfile
```
- Dockerfile content
    - `FROM`: the image build is based on, here is FROM mcr.microsoft.com/dotnet/core/aspnet, version is 6.0
    - `WORKDIR`: your app work folder in docker
    - `ENTRYPOINT`: how to start your program
    - We did not copy any program files to docker, means we will map our outer folder to docker foler such as `app` work folder
```
FROM mcr.microsoft.com/dotnet/core/aspnet:6.0
WORKDIR /app
ENTRYPOINT ["dotnet", "netcoreapp.dll"]  
```
# Publish and Build image with Dockerfile
* the tag can be ignore, this is using the default tag: latest
* aspnetcore program in docker use port 80 default, this port will be exposed to outer
* we use port 8888 in outer to map port 80 in docker
* we map the publish folder to `app` work folder in docker
```
dotnet publish -c Release -f net6.0
cd yourpublishfolder
docker volume create dv_netcoreapp
docker build -t netcoreapp:latest .
docker run --name netcoreapp_8888 -p 8888:80 -v dv_netcoreapp:/data -itd netcoreapp:latest
```

# Publish and Build image with Docker Compose
```
docker compose -f docker-compose.yml up netcoreapp -d
```

# Check your docker WORKDIR is map to your publish folder:
```
docker exec -it netcoreapp_8888 bash
youcan use command `ls` here to check your program files
```
# Access your api
* http://localhost:8888/swagger/index.html


# build docker image under sln folder:
```
docker build -f netcoreapp/Dockerfile -t netcoreapp:latest .
```

# start the container with env
```
docker volume create dv_netcoreapp_v1
docker run --name netcoreapp_8889 --env-file .env_netcoreapp -p 8889:80 -v dv_netcoreapp_v1:/data -itd netcoreapp:latest
```